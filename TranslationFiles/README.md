# GarudaOS FIRE Translations
Each file holds the translations for a language, corresponding to that language's locale. For example, the file `translations_en.json` holds the translations for the English language. Each file uses the JSON format, meaning every key value contains the translation for that key. Thus, all keys are the same in each file, with differing key *values* depending on the language being translated in the file.

## Add Translations
1. While not necessary, installing the package `i18n-editor` from the AUR is highly recommended, as it will allow you to view the default text for each key value, the translations made thus far, and a box for you to input your new translation. If you would rather not install a separate application for translating though, a nearly identical application has been made into an AppImage [here](https://www.electronjs.org/apps/i18n-manager) that can work for this as well. You could of course ignore the above options and just manually create a file and add the JSON code directly, but that sounds like a pain and like quite a lot of unnecessary work. Why do all that when a small little package or AppImage can manage that part for you. For example, here is a screenshot.
![](Screenshots/i18n-editor.png)

2. Git clone the repository using the following command: ```git clone https://gitlab.com/garudaos-team/garudaos-fire.git```
3. If you installed the package:
   - Open up `i18n-editor` and select **File -> Import Project...** and then select the `Translations` directory.

   If you downloaded the AppImage:
   - Ensure the file has been marked as executable, and then click on it to launch the application. Once open, click **File -> Open Folder**, selecting the `Translations` directory. When the directory loads in the application, you will see warnings about duplicates. Simply ignore this, as it is confused by the presence of our `translations.json` file which acts as the default text file to grab from if the user's locale is not yet supported.
4. You should now see all the created translations.  
If you installed the package:
   - To create a translation of your own, simply select **Edit -> Add Locale...**, selecting the locale of the language you wish to create a translation for.

   If you downloaded the AppImage:
   - To create a translation of your own, simple open the `Translations` folder in your file manager and create a new file there called `translations_{LOCALE_NAME_OF_LANGUAGE}.json`
5. Translate away in your selected application!
