# Garuda Maintenance App Trigger Handlers
# Created by Azaiel for GarudaOS
# This is an open-source project using Python3.  Feel free to use
# what you'd like, but please give credit!  Improvements are always welcome!
# GarudaOS Discord: Azaiel

# This ensures that the Gtk version is 3.0
import subprocess
import gi
import json
import os
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
gi.require_version('Notify', '0.7')
from gi.repository import Notify
Notify.init("Unnecessary Packages")
try:
    import httplib
except:
    import http.client as httplib

# Check for Internet connection
conn = httplib.HTTPConnection("www.google.com", timeout=5)
try:
    conn.request("HEAD", "/")
    conn.close()
except:
    conn.close()
    Notify.Notification.new("Lacking Internet connection. The following operations may not work").show()

# Create variables for both the current working directory and the location of the settings file
workingDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))
gladeFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Glade', 'GarudaDowngrade.glade'))
bashFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Bash', 'Maintenance.sh'))
settingsFile = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Settings', 'settings.json'))
localeDirectory = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'TranslationFiles'))

# Create Handlers (Triggers) for each item
class Handler:
    with open(settingsFile) as outfile:
        self.settings = json.load(outfile)

# Close the window
    def onDestroy4(self, *args):
        Gtk.main_quit()

################################################################################
############################### Buttons ########################################
################################################################################

# Save Program List
    def onEntryDowngrade(self, pkgtxt):
        global enteredText
        enteredText = pkgtxt.get_text()
        print("Entered Text: ", enteredText)
        print()

# Recover From
    def onDowngrade(self, button):
        print("Entered Text: ", enteredText)
        Notify.Notification.new("Downgrading...").show()
        os.system('bash ' + bashFile + ' Downgrade ' + enteredText + ' ' + self.settings["terminal"])
        Gtk.main_quit()

################################################################################
############################### Drawing App Window #############################
################################################################################

builder = Gtk.Builder()
builder.add_from_file(gladeFile)
builder.connect_signals(Handler())

# Set Labels
with open(localeDirectory + '/translations_' + os.getenv('LANG').split('_')[0] + '.json') as json_file:
    locale = json.load(json_file)
    builder.get_object("PackageDowngradeButton").set_label(locale["GarudaOSFIRE"]["SystemTasksTab"]["RepairTab"]["DowngradeDialog"]["DowngradeButton"])
    builder.get_object("InsertPackageToDowngrade").set_placeholder_text(locale["GarudaOSFIRE"]["SystemTasksTab"]["RepairTab"]["DowngradeDialog"]["DowngradeInsert"])

window4 = builder.get_object("Garuda4")
window4.show_all()

Gtk.main()

Notify.uninit()
