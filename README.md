# GarudaOS FIRE (Features and Improvements in GarudaOS made Easy)

===== Notes by Rafael =====

Clone this repo:

```
git clone https://gitlab.com/garudaos-team/garudaos-special-system-files/garudaos-fire-mod.git
```

How do we generate GarudaOS FIRE once the repository is cloned?

GarudaOS FIRE requieres the file **garudaos-fire.tar.gz**

If we have modified the existing repository, then must delete the file **garudaos-fire.tar.gz**

**NOTE**: Remember to change the version number of GarudaOS FIRE.

How create the new file **garudaos-fire.tar.gz**?

Into garudaos-fire-mod directory, run from terminal:

```
tar -czv --exclude='PKGBUILD' --exclude='README.md' -f garudaos-fire.tar.gz *
```

Update the sums:

```
updpkgsums
```

Then compile:

```
makepkg
```

Ready!

===== End notes by Rafael =====

## Purpose
To make tasks that are commonly more advanced (need interaction with a terminal for) possible for a newbie who may not be comfortable in an Arch-based world at first.

### Functions

- Easily manage the most common display managers (LightDM and SDDM) which lack simple GUI management applications.
![](Screenshots/DisplayManagers.png)

- Install additional DEs if so desired.
![](Screenshots/DesktopEnvironments.png)

- Clean out your system using `pacman`-specific commands. For instance, cleaning the package cache and removing unneccesary (often orphaned) packages.
![](Screenshots/SystemMaintenance.png)
![](Screenshots/Repair.png)

- Rollback your entire system to a previous date.
![](Screenshots/Rollback.png)

- Expose the user to a few simple applications currently found in the AUR that can be used to make tasks easier in Arch Linux.
![](Screenshots/Addons.png)
![](Screenshots/Tools.png)

### Install

#### GarudaOS
1) Open up your favorite Software Installer (most likely Pamac) and search for "garuda-updates", clicking `Install`. If you prefer a terminal, just enter the below text:
```
sudo pacman -S garudaos-fire --noconfirm
```
DONE!

#### Arch Linux (or any derivative)
1) Navigate to our Gitlab page here (which you are obviously already at since you are reading this) by the following URL: https://gitlab.com/garuda-os-team/garuda-updates-and-maintenance

2) Download the `garudaos-fire` package (the only file ending in .zst).

3) Install the downloaded file either by right-clicking on it and selecting to "Open With..." your favorite Software Installer, or just through the terminal using the below command:
```
sudo pacman -U ${PATH_TO_FILE}
```
4) A new version came out since you last did this? No problem! If you are using a non-GarudaOS derivitive of Arch Linux (or Arch itself), GarudaOS FIRE will automatically determine whether or not an update is available, and update for you when you next launch it if necessary.
